use std::clone;

use bft_types::Program;
use num_traits::Num;

/// This struct represents the brainfuck virtual machine, with a tape, a pointer to the current
/// location on the tape. This struct can represent a VM with either a fixed length or growable
/// tape
///
/// The type parameter N represents the type of number used for the tape
#[derive(Debug)]
pub struct Vm<N>
where
    N: Num + clone::Clone,
{
    tape: Vec<N>,
    pointer: usize,
    growable: bool,
}

impl<N: Num + Clone> Vm<N> {
    /// Creates a new VM with a tape length of len, which is growable is the bool passed is true
    /// If the len passed is 0, the default value of 30000 will be used
    pub fn new(len: usize, growable: bool) -> Self {
        let mut default_len = 30000;
        if len != 0 {
            default_len = len
        }
        Self {
            tape: vec![num_traits::zero(); default_len],
            pointer: 0,
            growable,
        }
    }

    pub fn interpret(self, program: &Program) {
        println!("Program: {:?}", program);
    }

    /// Get a reference to the vm's tape.
    pub fn tape(&self) -> &[N] {
        self.tape.as_ref()
    }

    /// Get the vm's pointer.
    pub fn pointer(&self) -> usize {
        self.pointer
    }

    /// Returns true if the vm's tape is growable.
    pub fn growable(&self) -> bool {
        self.growable
    }
}
