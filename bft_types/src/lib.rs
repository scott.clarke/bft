//! This crate contains definitions of types which are to be used as part of a brainfuck
//! interpreter
use std::{fs, io::Result, path::Path};

/// This enum represents the eight brainfuck instructions
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum RawInstruction {
    Increment,
    Decrement,
    LoopOpen,
    LoopClose,
    RightShift,
    LeftShift,
    Output,
    Input,
}

impl RawInstruction {
    fn from_char(c: char) -> Option<RawInstruction> {
        match c {
            '+' => Some(Self::Increment),
            '-' => Some(Self::Decrement),
            '[' => Some(Self::LoopOpen),
            ']' => Some(Self::LoopClose),
            '>' => Some(Self::RightShift),
            '<' => Some(Self::LeftShift),
            '.' => Some(Self::Output),
            ',' => Some(Self::Input),
            _ => None,
        }
    }
}

impl std::fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Increment => write!(f, "Increment value at pointer"),
            Self::Decrement => write!(f, "Decrement value at pointer"),
            Self::LoopOpen => write!(f, "Start looping"),
            Self::LoopClose => write!(f, "Stop looping"),
            Self::RightShift => write!(f, "Increment pointer"),
            Self::LeftShift => write!(f, "Decrement pointer"),
            Self::Output => write!(f, "Output byte at pointer"),
            Self::Input => write!(f, "Read byte of input to location at pointer"),
        }
    }
}

/// This struct represents a raw brainfuck instruction, including the line and column numbers of
/// the instruction
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct RawInstructionStruct {
    raw_instruction: RawInstruction,
    line: i64,
    column: i64,
}

impl RawInstructionStruct {
    /// Create a new instance of this struct with the given instruction and line and cloumn numbers
    pub fn new(raw_instruction: RawInstruction, line: i64, column: i64) -> Self {
        Self {
            raw_instruction,
            line,
            column,
        }
    }

    /// Get the underlying RawInstruction
    pub fn raw_instruction(&self) -> RawInstruction {
        self.raw_instruction
    }

    /// Get the line number of this instruction
    pub fn line(&self) -> i64 {
        self.line
    }

    /// Get the cloumn number of this instruction
    pub fn column(&self) -> i64 {
        self.column
    }
}

/// This struct represents and full brainfuck program, consisting of the name of the file which the
/// program was loaded from, as well as a list of all of the instructions in the program
#[derive(Debug)]
pub struct Program {
    filename: String,
    instructions: Vec<RawInstructionStruct>,
}

impl Program {
    /// Create a new program from a filename and list of instructions
    pub fn new<T: AsRef<Path>>(filename: T, instructions: Vec<RawInstructionStruct>) -> Self {
        Self {
            filename: filename.as_ref().to_str().unwrap_or_default().to_string(),
            instructions,
        }
    }

    /// Create a new program from an existing file on the filesystem
    pub fn from_file<T: AsRef<Path>>(filename: T) -> Result<Self> {
        let contents = fs::read_to_string(&filename)?;
        let bf = contents.trim_end().split('\n');

        let mut raw_instruction_vec = Vec::new();
        let mut line = 1;
        for s in bf {
            let mut column = 1;
            // For each character in a line
            for c in s.chars() {
                // If it is a bf char, create struct and add to Vec
                let raw_instruction = match RawInstruction::from_char(c) {
                    Some(r) => r,
                    None => {
                        // If not bf char, skip
                        column += 1;
                        continue;
                    }
                };
                raw_instruction_vec.push(RawInstructionStruct::new(raw_instruction, line, column));
                column += 1;
            }
            line += 1;
        }
        Ok(Self::new(filename, raw_instruction_vec))
    }

    pub fn is_balanced(&self) -> bool {
        let mut unclosed_loops = 0;
        for instruction in self.instructions.iter() {
            if instruction.raw_instruction() == RawInstruction::LoopOpen {
                unclosed_loops += 1;
            } else if instruction.raw_instruction() == RawInstruction::LoopClose {
                if unclosed_loops > 0 {
                    unclosed_loops -= 1;
                } else {
                    return false;
                }
            }
        }
        unclosed_loops == 0
    }

    /// Get the filename of the program
    pub fn filename(&self) -> &str {
        self.filename.as_ref()
    }

    /// Get the instructions as a slice
    pub fn instructions(&self) -> &[RawInstructionStruct] {
        self.instructions.as_slice()
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use crate::{Program, RawInstructionStruct};

    #[test]
    fn filename() {
        let mut filename = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        filename.push("resources/test/testfile");
        let result = Program::from_file(&filename).unwrap();
        assert_eq!(result.filename(), filename.to_str().unwrap());
    }

    #[test]
    fn instructions() {
        let mut filename = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        filename.push("resources/test/testfile");
        let result = Program::from_file(&filename).unwrap();
        let expected = vec![
            RawInstructionStruct::new(crate::RawInstruction::Increment, 1, 1),
            RawInstructionStruct::new(crate::RawInstruction::Decrement, 1, 2),
            RawInstructionStruct::new(crate::RawInstruction::LoopOpen, 2, 1),
            RawInstructionStruct::new(crate::RawInstruction::LoopClose, 2, 6),
            RawInstructionStruct::new(crate::RawInstruction::Output, 3, 2),
            RawInstructionStruct::new(crate::RawInstruction::Input, 3, 3),
            RawInstructionStruct::new(crate::RawInstruction::RightShift, 4, 1),
            RawInstructionStruct::new(crate::RawInstruction::LeftShift, 4, 4),
        ];

        let mut i = 0;
        for element in result.instructions().iter() {
            assert_eq!(element, &expected.as_slice()[i]);
            i += 1;
        }
    }
}
