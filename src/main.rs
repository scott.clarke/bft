//! This binary crate takes the name of a file containing an brainfuck program and runs the program
//! using an internal VM.

mod cli;

use std::process::exit;

use bft_interp::Vm;
use clap::StructOpt;

type BftResult = Result<(), Box<dyn std::error::Error>>;

fn run_bft(options: cli::Options) -> BftResult {
    let program = bft_types::Program::from_file(&options.program)?;
    if !program.is_balanced() {
        return Err(format!(
            "Error in input file {}: The square brackets are unbalanced",
            options.program.to_str().unwrap_or("UNKNOWN INPUT FILE")
        )
        .into());
    }
    let cells = options.cells.unwrap_or_default();
    let vm: Vm<usize> = Vm::new(cells, options.growable);
    vm.interpret(&program);
    Ok(())
}

fn main() {
    let result = run_bft(cli::Options::parse());
    match result {
        Err(e) => {
            eprintln!("bft: {}", e);
            exit(1);
        }
        Ok(_) => exit(0),
    }
}
