use clap::Parser;
use std::path::PathBuf;

#[derive(Debug, Parser)]
#[clap(author, version)]
pub struct Options {
    /// The brainfuck program to execute
    #[clap(parse(from_os_str))]
    pub(crate) program: PathBuf,

    #[clap(short = 'c', long = "cells", value_name = "CELLS", parse(try_from_str))]
    pub(crate) cells: Option<usize>,

    #[clap(short = 'g', long = "growable")]
    pub(crate) growable: bool,
}
